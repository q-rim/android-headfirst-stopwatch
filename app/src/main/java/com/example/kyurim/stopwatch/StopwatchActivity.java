package com.example.kyurim.stopwatch;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class StopwatchActivity extends Activity {

    private static final String TAG = "---StopwatchActivity:";

    private int seconds = 0;
    private boolean isRunning, wasRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()*");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);

        // Retrieve Activity State
        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");             // restoring Int
            isRunning = savedInstanceState.getBoolean("isRunning");     // restoring Boolean
            wasRunning = savedInstanceState.getBoolean("wasRunning");   // restoring Boolean
        }

        runTimer();     // start the timer when the app starts
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart()*:: wasRunning = " + wasRunning);
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume()*");
        super.onResume();
        if (wasRunning == true) {   // start the clock at start if it was running before.
            isRunning = true;
        }

        // Debugging - count 3 seconds
        try {
            for (int i = 1; i <= 3; i++) {
                Log.i(TAG, "onResume()*:: Pause: " + i + "-sec");
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause()*");
        super.onPause();
        if (isRunning == true) {    // check to see if the stopwatch was running before stopping.
            wasRunning = true;
        }
        isRunning = false;      // get stopwatch to stop when onStop() method is called.
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop()*::  isRunning = " + isRunning);
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, "onRestart()*");
        super.onRestart();
//        isRunning = true;       // get stopwatch to resume when onRestart() method is called.
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()*");
        super.onDestroy();
    }

    // Save Activity State
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("seconds", seconds);
        savedInstanceState.putBoolean("isRunning", isRunning);
        savedInstanceState.putBoolean("wasRunning", wasRunning);
    }

    public void onClickStart(View view) {
        Log.i(TAG, "onClickStart()");
        isRunning = true;
    }

    public void onClickStop(View view) {
        Log.i(TAG, "onClickStop()");
        isRunning = false;
    }

    public void onClickReset(View view) {
        Log.i(TAG, "onClickReset()");
        isRunning = false;
        seconds = 0;
    }

    public void runTimer() {
        final TextView timeView = (TextView) findViewById(R.id.time_TextView);
        final android.os.Handler handler = new android.os.Handler();        // create Handler
        handler.post(new Runnable() {               // call handler.post() - Allows the code in Runnable to run almost immediately.
            @Override
            public void run() {
                // Time
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;
                String time = String.format(Locale.getDefault(), "%d:%02d:%02d", hours, minutes, secs);     // Time formatting
                timeView.setText(time);

                if (isRunning) {
                    seconds++;                      // if the clock is running, then increment seconds
                }
                handler.postDelayed(this, 1000);    // handler.postDelayed() - Post the code in Runnable to be run again after 1 sec delay.
                Log.i("TIME", "TIME=" + time);         // output time
            }
        });
    }
}
